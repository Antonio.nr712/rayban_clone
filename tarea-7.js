playElement = document.getElementById('play')
muteElement = document.getElementById('mute')
video = document.getElementById('video')
playControl = false
muteControl = true
playIcon = document.getElementById('playIcon')
muteIcon = document.getElementById('muteIcon')

function play(){
    if(playControl){
        video.pause()
        playControl = false
        playIcon.setAttribute("name","pause-outline")
    
    }else{
        playControl = true
        playIcon.setAttribute("name","play-outline")
        
        video.play()
    }
 
}

function mute(){
    if(muteControl){
       
        muteControl = false
        
        muteIcon.setAttribute("name","volume-high-outline")
    
    }else{
      
        muteControl = true
        video.muted= muteControl
        muteIcon.setAttribute("name","volume-mute-outline")
       
    }
}