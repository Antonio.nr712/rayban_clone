const texts = ["Consulta los términos completos de nuestras promociones aquí", "Envío Gratis, Desde Italia, en Todos los Pedidos"];
let currentIndex = 0;

function changeText() {
    const centerTextElement = document.getElementById("center-text");
    centerTextElement.classList.remove('opacity-100');
    centerTextElement.classList.add('opacity-0');
            
    setTimeout(() => {
        centerTextElement.textContent = texts[currentIndex];
        centerTextElement.classList.remove('opacity-0');
        centerTextElement.classList.add('opacity-100');
        currentIndex = (currentIndex + 1) % texts.length;
    }, 1000);
}

setInterval(changeText, 10000);